/**
 * ���������� ������
 */
function setup_arrow(team, pos_x, pos_y, offset_real_pos_y, skin){
    
    //��� pixel js
    this.pos = { x: pos_x, y: (pos_y-offset_real_pos_y)};
    this.size = { width: 12, height: 16 };
    this.asset = new PixelJS.AnimatedSprite();
    this.velocity = { x: 200, y: 200 };
    this.team = team;
    if (team=="left") {
        this.asset.prepare({
            name: skin+'_left.png',
            frames: 1,
            rows: 1,
            speed: 100,
            defaultFrame: 0
        });
    }else{
        this.asset.prepare({
            name: skin+'_right.png',
            frames: 1,
            rows: 1,
            speed: 100,
            defaultFrame: 0
        }); 
    }
    
    
    //END ��� pixel.js
    
    this.offset_real_pos_y = offset_real_pos_y;
    this.pos_x = pos_x; //���������� x - ���������� ����� ���������� �� ��������� ��������
    this.pos_y = pos_y; //���������� y
    this.condition = 'move';
    this.arrow_end = false;
    this.type = 'arrow';
    this.width_x = 20;
    this.width_y = 10;

    this.move = function(){
        //���������, ���� ������ ������������� �� ��� ���� ���, ������� �� ��� ����-������
        if(this.team == "left"){

            //������ ����� �������
            var x_min = this.pos_x;
            var x_max = this.pos_x + this.width_x + this.speed;
            var y_min = this.pos_y - this.width_y;
            var y_max = this.pos_y;
            var units = itemLayer._components;
            for(var i = 0; i<units.length; i++){
                var unit = units[i];
                if(unit.team == 'right'&&unit.action!='death'&&unit.type!='arrow'){
    
                    if(!check_intersection(
                            unit.pos_x, unit.pos_x+unit.width_x,
                            unit.pos_y - unit.width_y, unit.pos_y,
                            x_min, x_max, 
                            y_min, y_max 
                       )){
                        //��������� ����
                        unit.damaged(this.damage);
                        this.arrow_end = true;
                        break;
                    }
    
                }
            }
            //���������� ������
            this.pos_x += this.speed;
        }else{
            //������ ������ �������
            var x_min = this.pos_x;
            var x_max = this.pos_x + this.width_x + this.speed;
            var y_min = this.pos_y - this.width_y;
            var y_max = this.pos_y;
            
            
            var units = itemLayer._components;
            for(var i = 0; i<units.length; i++){
                var unit = units[i];
                if(unit.team == 'left'&&unit.action!='death'&&unit.type!='arrow'){
                    if(!check_intersection(
                            unit.pos_x, unit.pos_x+unit.width_x,
                            unit.pos_y - unit.width_y, unit.pos_y,
                            x_min, x_max, 
                            y_min, y_max 
                       )){
                        //��������� ����
                        unit.damaged( this.damage);
                        this.arrow_end = true;
                        break;
                    }      
                }
            }
            //���������� ������
            this.pos_x -= this.speed;
        }
        if(this.pos_x>1200||this.pos_x<-100){
            this.arrow_end = true;
        }
        this.refresh_pos_custom_move();
    }
    /**
     * ��������� ������� ����� ������������
     */
    this.refresh_pos_custom_move = function(){
        this.pos.x = this.pos_x;
        this.pos.y  = this.pos_y - this.offset_real_pos_y;
    }
}









