/* �������� ��������� ���� */

var config, raw_params, params, main_loop, itemLayer, ai, changeScore, random, gods;

// ������ ��������� ���������
params = {
  right    : 'zombie',
  left     : 'marvel',
  location : 'fon-1'
};

// �������������� ���������
raw_params = document.location.href.split('?').pop().split('&');
for(p in raw_params) {
  var param_data = raw_params[p].split('=');

  params[param_data.shift()] = param_data.shift();
}

// �������� ������
config = {
  //������� ������ � ��������
  frame_time: 0.050,
  //��� ����
  background_src: params.location+'.jpg',
  // ��������� ����
  "user-score": 100,
  // ������ ���
  "left_god": params.left,
  // �� ����� ������ ���
  "right_god": params.right
};

// ��������� �����
changeScore = function(delta){

  config['user-score'] += delta;

  if(config['user-score'] < 0)
    config['user-score'] = 0;

  console.log('Score: '+config['user-score']);
};

// �����-������������
random = function getRandomArbitary(min, max) {
  min = min || 0;
  max = max || 100;

  return Math.random() * (max - min) + min;
};

// ������ ����� � �������
gods = {
  jesus    : [],
  zombie   : ["vamp", "zombie_unit"],
  starwars : ["ioda", "vader", "jedi_1", "jedi_2", "chubaka", "droid"],
  marvel   : ["batman", "spiderman", "superman"],
  pirate   : [],
  robin    : [],
  kukuruza : []
};


window.onload  = function () {
    if (document.readyState == "complete") {
        var game = new PixelJS.Engine();
        game.init({
            container: 'game_container',
            width: 1200,
            height: 675
        });
        
        //================
        //������ ���
        var backgroundLayer = game.createLayer('background');
        var grass = backgroundLayer.createEntity();
        backgroundLayer.static = true;
        grass.pos = { x: 0, y: 0 };
        grass.asset = new PixelJS.Tile();
        grass.asset.prepare({
            name: config['background_src'],
            size: { 
                width: 1200, 
                height: 675 
            }
        });
        //END ������ ���
        //================
        
        //�������� ���� ������
        itemLayer = game.createLayer('items');

        //������� �����, ������� �� ����� ���������
        var coin = itemLayer.createEntity();
        setup_unit.apply(coin, ["left", 0 , 100, 82]);

        
        var control_unit = new Control_unit(coin);
        game.on('keyDown', function (keyCode) {
            control_unit.key_handler_keydown(keyCode);
        });
         
        game.on('keyUp', function (keyCode) {
            control_unit.key_handler_keyup(keyCode);
        });
        
        
        //������� �������� ����
        main_loop = new Main_loop(game);
        //��������, ������������� �������� 0.0.0.-1
        ai = new Ai();

        // ���������� ������ �� �����
        var unit, side, side_param, addingUnit;

        addingUnit = function(side){

          side = side || (parseInt(random(0,100)) % 2 == 0)?"left":"right";

          side_param = {
            left: {
              start_x: random(-100, 100),
              start_y: 675-random(0, 650)
            },
            right: {
              start_x: 1200-random(100, -100),
              start_y: 675-random(0, 650)
            }
          };

          // todo: ����� ����� ���������� ��������� ������ �� ������ ����


          console.log('Unit '+side+' reborn');

          unit = itemLayer.createEntity();
          setup_unit.apply(unit, [side, side_param[side].start_x, side_param[side].start_y, 82]);

          ai.controlling_unit.push(unit);
        };

        for(i=0; i<20; i++)
        {
          addingUnit();
        }



        var time_passed_frame = 0;
        game.loadAndRun(function (elapsedTime, dt) {
            time_passed_frame+=dt;
            if (time_passed_frame>config['frame_time']) {
                time_passed_frame -= config['frame_time'];
                control_unit.check_control();
                main_loop.step();
            }

        });
    }
}






