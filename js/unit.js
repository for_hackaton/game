/**
 * ����� ������� ������ ����
 */

function setup_unit(team, pos_x, pos_y, offset_real_pos_y){
    //��� pixel js
    var max_pos = 0;
    this.pos = { x: pos_x, y: (pos_y-offset_real_pos_y)};
    this.size = { width: 12, height: 16 };
    this.asset = new PixelJS.AnimatedSprite();
    this.velocity = { x: 200, y: 200 };
    if (team=="left") {
        max_pos = gods[config.left_god].length - 1;
        this.asset.prepare({
            name: gods[config.left_god][parseInt(random(0, max_pos))]+'.png',
            frames: 8,
            rows: 6,
            speed: 100,
            defaultFrame: 0
        });
    }else{
        max_pos = gods[config.right_god].length - 1;
        this.asset.prepare({
          name: gods[config.right_god][parseInt(random(0, max_pos))]+'.png',
            frames: 8,
            rows: 6,
            speed: 100,
            defaultFrame: 0
        }); 
    }
    
    
    //END ��� pixel.js
    this.ai_accent = 1; // ������� ���
    this.speed = 10;
    this.offset_real_pos_y = offset_real_pos_y;
    this.type = 'unit';
    this.name = 'no name'; //���
    this.health = 100; //�������� �����
    this.skin_src = 'default.png'; //��������
    this.pos_x = pos_x; //���������� x - ���������� ����� ���������� �� ��������� ��������
    this.pos_y = pos_y; //���������� y
    
    this.width_x = 20; // ������ ������� - ������ ���������� �� ������ ��������
    this.width_y = 20; // ������ �����
    //this.speed = 5; //�������� ����������� �����
    this.moveToSideX = 0;
    this.moveToSideY = 0;
    this.condition = 'stand'; //������� ��������� �������
    
    this.action = 'none'; //������� �������� �������, �������� ����� ���������� � condition
    this.team = team; //� ����� ������� ���������� ���� - left, right
    this.god = 'none'; //����� ���
    
    this.attack_radius = 50; //��������� ������� ����� �����
    this.attack_speed = 10; //�������� ����� (����� ������� �� ������� ����)
    this.attack_condition = 0; //��������� ����� (��� ������ ��������� ����� ��������� attack_speed - ���������� ����)
    this.attack_damage = 20; //���� ����� �������� ���
    
    
    this.attack_distance_speed = 10; //�������� ����� (����� ������� �� ������� ������)
    this.attack_distance_condition = 0; //���������� ������� �����
    this.attack_distance_damage = 50; //���� ����� �������� ���
    this.attack_distance_speed_arrow = 20; //�������� ������ ������
    
    /**
    * ���������, ���� ������������
    */
    this.moveTo = function (moveTo){
        switch(moveTo){
            case 'up':
                this.moveToSideY = -1;
                break;
            case 'down':
                this.moveToSideY = 1;
                break;
            case 'left':
                this.moveToSideX = -1;
                break;
            case 'right':
                this.moveToSideX = 1;
                break;
        }
    }
    /**
     * �����������
     */
    this.move = function(){

        if(this.condition=='death' || this.condition == 'winner'){
            return;
        }

        //�� ���� ��������� �������� ����������� ����� �� 5 �����, � ������ ������ ������� ���
        if((this.moveToSideX == 0) && (this.moveToSideY == 0)){
            //���� ����� �� �����
            this.condition = 'stand';
            this.asset.row = 0;
            return;
        }
        //���� ��������
        this.condition = 'move';
        this.asset.row = 1;
         if((!this.moveToSideX == 0) && (!this.moveToSideY == 0)){
        //���� ��������� �� ���������
        
        //��������� ����� �� ������������� ���� �� ��� ����������
        //���� �� �����, �� ������ �� ����������
        var collision;
        collision = check_diagonal_move_unit(this, 0.7071*this.moveToSideX*this.speed, 0.7071*this.moveToSideY*this.speed);
        if(!collision){
            this.pos_x +=  0.7071*this.moveToSideX*this.speed;
            this.pos_y +=  0.7071*this.moveToSideY*this.speed;
        }
    }else{
        //��������� ����� �� ������������� ���� �� ��� ����������
        var collision;
        if(this.moveToSideY==0){
            collision = check_horizontal_move_unit(this, this.moveToSideX*this.speed);
        }else{
            collision = check_vertical_move_unit(this, this.moveToSideY*this.speed);
            
        }
        //���� ��������� � ����� �����������
        if(collision[0]){
            if(collision[1]>1){
                this.pos_x += this.moveToSideX*(collision[1]-1);
                this.pos_y += this.moveToSideY*(collision[1]-1);
            }
        }else{
            this.pos_x += this.moveToSideX*this.speed;
            this.pos_y += this.moveToSideY*this.speed;
        }
    }
    //��� ��� ��� pixel js ������ ���������� - ��������� ��
    this.refresh_pos_custom_move();
    
    //������� �������� ��� ���������� ����
    this.moveToSideX = 0;
    this.moveToSideY = 0;
    }
    /**
     * ���������, ��� ����� �������
     */
    this.set_action = function(action){
        
        if(this.action=='death'){
            return;
        }
        switch(action){
            case 'attack':
                
                this.action = 'attack';
                break;
            case 'attack_distance':
                
                this.action = 'attack_distance';
                break;
    
        }
        
    }
    /**
     * ��������
     */
    this.do_action = function(){
        switch(this.action){
            case 'attack':
                this.asset.row = 2;
                this.condition = 'attack';
                this.attack();
                break;
            case 'attack_distance':
                this.asset.row = 3;
                this.condition = 'attack_distance';
                this.distance_attack();
                break;
        }
        this.action = 'none';
    }
    
    /**
     * ���� �������
     */
    this.attack = function(){
        /**
         * ����, ���� �� � ������� ����� ����� ����
         *
         * ��� ����� ������� 
         * pos_x , pos_y - ������ ����� ����������
         * pos_x+width_x, pos_y - ������ ������ ����������
         *
         * �.�. ������ �������� � ������ �� ��� Y ������� �� ���������
         * pos_x+width_x, pos_y - width_y - ������� ������ ����������  ������� (� �� ��������!!!!)
         * 
         * ������� ������� ����� ������������� ABCD
         * A) pos_x+width_x, pos_y - width_y - attack_radius/2    B) pos_x+width_x+attack_radius, pos_y - width_y 
         * D) pos_x+width_x, pos_y+attack_radius/2                C) pos_x+width_x+attack_radius, pos_y   
         *
         * ���������, ������ ����� �������������� ���� �� ������� ����� ������ ������
         * ���� ���� - ���������� ������� �����
         * 
         */
        this.attack_condition++;
        if(this.attack_condition<this.attack_speed){
            return;
        }
        this.attack_condition = 0;
        if(this.team == "left"){
            //���� ����� �������
            var x_min = this.pos_x + this.width_x;
            var x_max = this.pos_x + this.width_x + this.attack_radius;
            var y_min = this.pos_y - this.width_y - this.attack_radius/2;
            var y_max = this.pos_y + this.attack_radius/2;
            var units = itemLayer._components;
            for(var i = 0; i<units.length; i++){
                var unit = units[i];
                if(unit.team == 'right'&&unit.action!='death'){
                    //��������� ��� ����� ����� �����
                    var x_dot = unit.pos_x;
                    var y_dot_top = unit.pos_y - unit.width_y;
                    var y_dot_bottom = unit.pos_y;
                    if(
                        ((x_min<=x_dot)&&(x_dot<=x_max))&&
                        (((y_min<=y_dot_top)&&(y_dot_top<=y_max))||
                         ((y_min<=y_dot_bottom)&&(y_dot_bottom<=y_max)))
                    ){
                        //��������� ����
                        unit.damaged(this.attack_damage);
                    }       
                }
            } 
        }else{
            //���� ������ �������
            var x_min = this.pos_x-this.attack_radius;
            var x_max = this.pos_x;
            var y_min = this.pos_y - this.width_y;
            var y_max = this.pos_y;
            var units = itemLayer._components;
            for(var i = 0; i<units.length; i++){
                var unit = units[i];
                if(unit.team == 'left'&&unit.action!='death'){
                    //��������� ��� ������ ����� �����
                    var x_dot = unit.pos_x+unit.width_x;
                    var y_dot_top = unit.pos_y - unit.width_y;
                    var y_dot_bottom = unit.pos_y;
                    if(
                        ((x_min<=x_dot)&&(x_dot<=x_max))&&
                        (((y_min<=y_dot_top)&&(y_dot_top<=y_max))||
                         ((y_min<=y_dot_bottom)&&(y_dot_bottom<=y_max)))
                    ){
                        //��������� ����
                        unit.damaged(this.attack_damage);
                    }       
                }
            }
            
            
        }
        
        
    }
    /**
     * ���� ������� �����������
     */
    this.damaged = function(damage){
        //������ �������� ��������
        this.health -= damage;
        console.log(this.health);
    
        //���������, ���� ��� "�����"
        if(this.health<=0)
        {
          if(this.team == 'right')
          {
            changeScore(1);
          }

            console.log("r.i.p. "+this.name);
            //r.i.p. this ����  
            this.condition = 'death';
            this.action = 'death';
            this.asset.row = 4;
        }
        
    }        
    /**
     * ������������� �����
     */
    
    this.distance_attack = function(){
        this.attack_distance_condition++;
        if(this.attack_distance_condition<this.attack_distance_speed){
            return;
        }
        this.attack_distance_condition = 0;
        
                
        
        /**
         * ������� ������, ������� �����, ���� �� ������� ������� ��� �� ����������� � �����������
         */
        var arrow = itemLayer.createEntity();
        setup_arrow.apply(arrow, [this.team, this.pos_x , this.pos_y, 40, "default_arrow"]);
        arrow.asset.load(arrow.asset._prepInfo);
        
        arrow.archer = this;
        arrow.damage = this.attack_distance_damage;
        arrow.speed = this.attack_distance_speed_arrow*Math.random()+10;
        
        
        
    }
    /**
     * ��������� ������� �����
     */
    this.set_pos = function(pos_x,pos_y){
        this.pos_x = pos_x; //���������� x - ���������� ����� ���������� �� ��������� ��������
        this.pos_y = pos_y; //���������� y
        this.pos = { x: pos_x, y: pos_y - this.offset_real_pos_y};
    }
    /**
     * ��������� ������� ����� ������������
     */
    this.refresh_pos = function(){
        this.pos_x  = this.pos.x;
        this.pos_y  = this.pos.y + this.offset_real_pos_y;
    }
    /**
     * ��������� ������� ����� ������������
     */
    this.refresh_pos_custom_move = function(){

        if(this.condition == 'winner')
          return false;

        this.pos.x = this.pos_x;
        this.pos.y  = this.pos_y - this.offset_real_pos_y;

        if(this.pos.x < 0)
        {
          if(this.team == 'right')
            this.setEnemyUnitWinner();
          else
            this.pos_x = 0;
        }

        if(this.pos.x > 1200 && this.team == 'left')
          this.setComradeUnitWinner();

    }

    this.setEnemyUnitWinner = function(){

      this.condition = 'winner';
      this.pos.x = -1000;

      changeScore(-1);
    };

    this.setComradeUnitWinner = function(){

      this.condition = 'winner';
      this.pos.x = 10000;

      changeScore(1);
    };
}