/**
 * ���������� ����������
 *
 */




/**
 * ���������� �������
 */
/**
 * ����� �������� �� ������������ �� �����������
 */
function check_horizontal_move_unit(this_unit, to_x){

    var y_min = this_unit.pos_y-this_unit.width_y;
    var y_max = this_unit.pos_y;
    if(to_x>0){
        var x_min = this_unit.pos_x+this_unit.width_x;
        var x_max = this_unit.pos_x+this_unit.width_x+to_x;
    }else{
        var x_min = this_unit.pos_x+to_x;
        var x_max = this_unit.pos_x;
    }
    var collision = false;
    var collision_distance = 99999;
    var units = itemLayer._components;
    for(var i = 0; i<units.length; i++){
            var unit = units[i];
            if(unit.action!='death'&&unit!==this_unit){
                var x2_min = unit.pos_x;
                var x2_max = unit.pos_x+unit.width_x;
                var y2_min = unit.pos_y - unit.width_y;
                var y2_max = unit.pos_y;
                if(
                    !check_intersection(x_min, x_max, y_min, y_max, x2_min, x2_max, y2_min, y2_max)
                ){
                    //��������� ����
                    collision = true;
                    //����������� ��������� �� �����
                    if(to_x>0){
                        if((Math.abs(x_min - x2_min))<collision_distance){
                            collision_distance = Math.abs(x_min - x2_min);
                        }
                    }else{
                        if((Math.abs(x_max - x2_max))<collision_distance){
                            collision_distance = Math.abs(x_max - x2_max);
                        }
                    }
                }       
            }
        } 
    
    return [collision, collision_distance];
    
}
/**
 *  ��� ���� ������� ��� �������� ������������ 
 */
function check_vertical_move_unit(this_unit, to_y){
     
   var x_min = this_unit.pos_x;
    var x_max = this_unit.pos_x+this_unit.width_x;
    if(to_y>0){
        var y_min = this_unit.pos_y;
        var y_max = this_unit.pos_y+to_y;
    }else{
        var y_min = this_unit.pos_y-this_unit.width_y+to_y;
        var y_max = this_unit.pos_y-this_unit.width_y;
    }
    var collision = false;
    var collision_distance = 99999;
    var units = itemLayer._components;
    for(var i = 0; i<units.length; i++){
            var unit = units[i];
            if(unit.action!='death'&&unit!==this_unit){

                var y2_min = unit.pos_y-unit.width_y;
                var y2_max = unit.pos_y;
                var x2_min = unit.pos_x;
                var x2_max = unit.pos_x+unit.width_x;
                
                if(
                    !check_intersection(x_min, x_max, y_min, y_max, x2_min, x2_max, y2_min, y2_max)
                ){
                    //��������� ����
                    collision = true;
                    //����������� ��������� �� �����
                    if(to_y>0){
                        if((Math.abs(y_min - y2_min))<collision_distance){
                            collision_distance = Math.abs(y_min - y2_min);
                        }
                    }else{
                        if((Math.abs(y_max - y2_max))<collision_distance){
                            collision_distance = Math.abs(y_max - y2_max);
                        }
                    }
                }       
            }
        } 
    
    return [collision, collision_distance];
}


/**
 * �������� ������������ �� ���������
 */
function check_diagonal_move_unit(this_unit, to_x, to_y){
   var x_min = this_unit.pos_x+to_x;
    var x_max = this_unit.pos_x+this_unit.width_x+to_x;
    var y_min = this_unit.pos_y-this_unit.width_y+to_y;
    var y_max = this_unit.pos_y+to_y;
    
    
    var units = itemLayer._components;
    for(var i = 0; i<units.length; i++){
        
        var unit = units[i];
        if(unit.action!='death'&&unit!==this_unit){
            var x2_min = unit.pos_x;
            var x2_max = unit.pos_x+unit.width_x;
            var y2_min = unit.pos_y - unit.width_y;
            var y2_max = unit.pos_y;
            if(!check_intersection(x_min, x_max, y_min, y_max, x2_min, x2_max, y2_min, y2_max)){
                return true;
            }
        }
    }
    
    
    return false; 
}




/**
 * �������� ����������� ���� ���������������
 */
function check_intersection(ob1_x_min, ob1_x_max, ob1_y_min, ob1_y_max, ob2_x_min, ob2_x_max, ob2_y_min, ob2_y_max){
     return ( ob1_y_max < ob2_y_min || ob1_y_min > ob2_y_max || ob1_x_max < ob2_x_min || ob1_x_min > ob2_x_max );
}


/**
 * �������� ����������� ���� ������
 */
function check_units_collision(unit_1, unit_2){
    console.log('test');
    var ob1_x_min = unit_1.pos_x;
    var ob1_x_max = unit_1.pos_x+unit_1.width_x;
    var ob1_y_min = unit_1.pos_y - unit_1.width_y;
    var ob1_y_max = unit_1.pos_y; 
    
    var ob2_x_min = unit_2.pos_x;
    var ob2_x_max = unit_2.pos_x+unit_2.width_x;
    var ob2_y_min = unit_2.pos_y - unit_2.width_y;
    var ob2_y_max = unit_2.pos_y; 
    
     return !( ob1_y_max < ob2_y_min || ob1_y_min > ob2_y_max || ob1_x_max < ob2_x_min || ob1_x_min > ob2_x_max );
}


/**
 * ������� ��� ���������� �� pos_y - ������� �������������� ������ �������
 */
function compareObjects (a, b) {
    if (a.pos_y > b.pos_y) return 1;
    if (a.pos_y < b.pos_y) return -1;
    return 0;
};
















