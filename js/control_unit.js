/**
 * ����� Control_unit
 */

function Control_unit(unit){
    this.unit = unit;

    this.keys = {
        //�������
        38 : ['up'   , false], 
        40 : ['down' , false], 
        37 : ['left' , false], 
        39 : ['right', false], 
        
        //wasd
        87 : ['up'   , false], 
        83 : ['down' , false], 
        65 : ['left' , false], 
        68 : ['right', false], 
        
        //�����
        32 : ['attack', false],
        //������������� ����� - �������� ������
        81 : ['attack_distance', false],
        //����
        69 : ['block', false],
    }
}

Control_unit.prototype.key_handler_keydown = function(keyCode) {
    if ( this.keys[keyCode] !== undefined){
        this.keys[keyCode][1] = true;
    }
     return false;
}

Control_unit.prototype.key_handler_keyup = function(keyCode) {
    if (this.keys[keyCode] !== undefined){
        this.keys[keyCode][1] = false;
    }
    
}


Control_unit.prototype.check_control = function(){

    for(var i in this.keys){
            if(this.keys[i][1]){
                switch(this.keys[i][0]){
                    case 'up':
                        this.unit.moveTo('up');
                        break;
                    case 'down':
                        this.unit.moveTo('down');
                        break;
                    case 'left':
                        this.unit.moveTo('left');
                        break;
                    case 'right':
                        this.unit.moveTo('right');
                        break;
                    case 'attack':
                        this.unit.set_action('attack');
                        break;
                    case 'attack_distance':
                        this.unit.set_action('attack_distance');
                        break;
                    case 'block':
                        this.unit.set_action('block');
                        break;
                }
            }
        }
}



