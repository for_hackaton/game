/**
 * ����� Ai 
 *
 */

function Ai(){
    this.controlling_unit = new Array();
}

/**
 * ��� ���� ���
 * �����������, ���� ����� �������, ������ ��������� ������ ������
 */
Ai.prototype.controlling_step = function(){
    for(var i=0; i<this.controlling_unit.length; i++){
        var unit = this.controlling_unit[i];
        if(unit.ai_block){
            unit.ai_block = false;
            continue;
        }
        if(unit.team=='left'){
            if(unit.ai_accent==1){
                //������� ���
                //��������� ���� �� � ����� ������� ����� ����� ����
                var move_now = true;
                var units = itemLayer._components;
                for(var j=0; j<units.length; j++){
                    if(units[j].team == 'right'&&units[j].action!='death'){
                        //��������� �� �����������
                        if(!check_intersection(
                                unit.pos_x+unit.width_x, unit.pos_x+unit.width_x+unit.attack_radius,
                                unit.pos_y-unit.width_y, unit.pos_y,
                                units[j].pos_x, units[j].pos_x+units[j].width_x,
                                units[j].pos_y-units[j].width_y, units[j].pos_y      
                            )){
                            //���� ��������� � ���� ���������
                            unit.set_action('attack');
                            move_now = false;
                            break;
                        }
                    }
                }
                if(move_now){
                    //������������
                    //�������� ����� ����� ��� ���� �����
                    var temp = Math.random();
                    if(temp>0.95){
                        unit.moveTo('up');
                    }else{
                        if(temp>0.9){
                            unit.moveTo('down');
                        }
                    }
                    unit.moveTo('right');
                }  
            }else{
                //������� ���
                 unit.set_action('attack_distance');
            }
        }
        //������ � ������ ��������
        if(unit.team=='right'){
            if(unit.ai_accent==1){
                //������� ���
                //��������� ���� �� � ����� ������� ����� ����� ����
                var move_now = true;
                var units = itemLayer._components;
                for(var j=0; j<units.length; j++){
                    if(units[j].team == 'left'&&units[j].action!='death'){
                        //��������� �� �����������
                        if(!check_intersection(
                                unit.pos_x-unit.attack_radius, unit.pos_x,
                                unit.pos_y-unit.width_y, unit.pos_y,
                                units[j].pos_x, units[j].pos_x+units[j].width_x,
                                units[j].pos_y-units[j].width_y, units[j].pos_y      
                            )){
                            //���� ��������� � ���� ���������
                            unit.set_action('attack');
                            move_now = false;
                            break;
                        }
                    }
                }
                if(move_now){
                    //������������
                    //�������� ����� ����� ��� ���� �����
                    var temp = Math.random();
                    if(temp>0.95){
                        unit.moveTo('up');
                    }else{
                        if(temp>0.9){
                            unit.moveTo('down');
                        }
                    }
                    unit.moveTo('left');
                }  
            }else{
                //������� ���
                unit.set_action('attack_distance');
            }
        }
        
        
        
    }
}